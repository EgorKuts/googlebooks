package com.petprojects.egor.googlebooksapi.adapters;

import com.petprojects.egor.googlebooksapi.model.pojo.Book;

import java.util.List;

public interface BooksAdapter {
    void addBooks(List<Book> books);
}
