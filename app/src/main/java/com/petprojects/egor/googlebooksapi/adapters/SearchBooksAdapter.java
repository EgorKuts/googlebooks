package com.petprojects.egor.googlebooksapi.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.petprojects.egor.googlebooksapi.R;
import com.petprojects.egor.googlebooksapi.model.pojo.Book;
import com.petprojects.egor.googlebooksapi.model.pojo.VolumeInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class SearchBooksAdapter extends RecyclerView.Adapter<SearchBooksAdapter.BookItemHolder> implements BooksAdapter{
    List<Book> books;
    ItemClickCallback itemClickCallback;
    Context context;

    public SearchBooksAdapter(Context context,ItemClickCallback clickCallback){
        books=new ArrayList<>();
        this.itemClickCallback=clickCallback;
        this.context=context;
    }

    @NonNull
    @Override
    public BookItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_item, parent, false);

        return new BookItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BookItemHolder holder, int position) {
        Book book=books.get(position);
        VolumeInfo volumeInfo=book.getVolumeInfo();

        holder.title.setText("Name: "+volumeInfo.getTitle());
        if(book.getSaleInfo().saleability.equals("FOR_SALE"))
        holder.bookPrice.setText("Price: "+book.getSaleInfo().retailPrice.amount +" "+book.getSaleInfo().retailPrice.currencyCode);
        else holder.bookPrice.setText("FREE");

        holder.language.setText("Language: "+ (new Locale(volumeInfo.getLanguage(), volumeInfo.getLanguage().toUpperCase())).getDisplayLanguage());
        holder.date.setText("Publish date: "+volumeInfo.publishedDate);

        String result=null;

        if(volumeInfo.getAuthors()!=null&&volumeInfo.getAuthors().size()>0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                result=volumeInfo.getAuthors().stream().collect(Collectors.joining(","));
            } else {
                StringBuilder strBuilder = new StringBuilder();

                for (String name : volumeInfo.getAuthors()) {
                    strBuilder.append(name);
                    strBuilder.append(",");
                }
                strBuilder.deleteCharAt(strBuilder.length() - 1);
                result=strBuilder.toString();
            }
            holder.author.setText("Authors: "+result);
        }

        if(volumeInfo.getImageLinks()!=null&&volumeInfo.getImageLinks().getSmallThumbnail()!=null) {
            Glide.with(context)
                    .load(volumeInfo.getImageLinks().smallThumbnail)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            e.printStackTrace();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(holder.img);
        }
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    @Override
    public void addBooks(List<Book> books) {
        this.books.clear();
        this.books.addAll(books);
        this.notifyDataSetChanged();
    }

    class BookItemHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView language;
        TextView author;
        TextView date;
        TextView bookPrice;

        ImageView img;

        public BookItemHolder(View itemView) {
            super(itemView);
            bookPrice =itemView.findViewById(R.id.bookPrice);
            author=itemView.findViewById(R.id.authorBook);
            title=itemView.findViewById(R.id.bookTitle);
            img=itemView.findViewById(R.id.bookPhoto);
            language=itemView.findViewById(R.id.bookLanguage);
            date=itemView.findViewById(R.id.publishDate);

            itemView.setOnClickListener(view -> itemClickCallback.click(books.get(getAdapterPosition())));
        }
    }

    public  interface ItemClickCallback{
        void click(Book book);
    }
}
