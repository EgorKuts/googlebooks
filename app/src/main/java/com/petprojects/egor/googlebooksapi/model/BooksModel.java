package com.petprojects.egor.googlebooksapi.model;

import com.petprojects.egor.googlebooksapi.model.pojo.GoogleBooksApiResponse;

import java.io.IOException;

public interface BooksModel {
    GoogleBooksApiResponse getBooks(String query) throws IOException;
}
