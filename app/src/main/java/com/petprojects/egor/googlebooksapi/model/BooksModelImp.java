package com.petprojects.egor.googlebooksapi.model;

import com.petprojects.egor.googlebooksapi.model.pojo.GoogleBooksApiResponse;
import com.petprojects.egor.googlebooksapi.model.service.GoogleBooksApiClient;
import com.petprojects.egor.googlebooksapi.model.service.GoogleBooksApiInterface;
import com.petprojects.egor.googlebooksapi.presenter.Presenter;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Response;

public class BooksModelImp implements BooksModel {
    @Inject Presenter presenter;

    public BooksModelImp(Presenter presenter){
        this.presenter=presenter;
    }

    //TODO: to replace getting result on Presenter's callback
    @Override
    public GoogleBooksApiResponse getBooks(String query) throws IOException {
        GoogleBooksApiInterface apiInterface= GoogleBooksApiClient.getClient().create(GoogleBooksApiInterface.class);
        Call<GoogleBooksApiResponse> responseCall=apiInterface.getBooks(query);

        Response response=responseCall.execute();

        if(response.isSuccessful())
        return (GoogleBooksApiResponse) response.body();
        else throw new RuntimeException();
    }
}
