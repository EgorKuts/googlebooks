
package com.petprojects.egor.googlebooksapi.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListPrice implements Serializable {

    @SerializedName("amount")
    @Expose
    public Double amount;
    @SerializedName("currencyCode")
    @Expose
    public String currencyCode;

}
