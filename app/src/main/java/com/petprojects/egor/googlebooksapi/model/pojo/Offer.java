
package com.petprojects.egor.googlebooksapi.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Offer implements Serializable {

    @SerializedName("finskyOfferType")
    @Expose
    public Integer finskyOfferType;
    @SerializedName("listPrice")
    @Expose
    public ListPrice_ listPrice;
    @SerializedName("retailPrice")
    @Expose
    public RetailPrice_ retailPrice;
    @SerializedName("giftable")
    @Expose
    public Boolean giftable;

}
