
package com.petprojects.egor.googlebooksapi.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PanelizationSummary implements Serializable {

    @SerializedName("containsEpubBubbles")
    @Expose
    public Boolean containsEpubBubbles;
    @SerializedName("containsImageBubbles")
    @Expose
    public Boolean containsImageBubbles;

}
