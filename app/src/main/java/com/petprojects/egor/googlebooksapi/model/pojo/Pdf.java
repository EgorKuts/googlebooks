
package com.petprojects.egor.googlebooksapi.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Pdf implements Serializable {

    @SerializedName("isAvailable")
    @Expose
    public Boolean isAvailable;
    @SerializedName("acsTokenLink")
    @Expose
    public String acsTokenLink;

}
