
package com.petprojects.egor.googlebooksapi.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReadingModes implements Serializable {

    @SerializedName("text")
    @Expose
    public Boolean text;
    @SerializedName("image")
    @Expose
    public Boolean image;

}
