
package com.petprojects.egor.googlebooksapi.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RetailPrice_ implements Serializable {

    @SerializedName("amountInMicros")
    @Expose
    public Double amountInMicros;
    @SerializedName("currencyCode")
    @Expose
    public String currencyCode;

}
