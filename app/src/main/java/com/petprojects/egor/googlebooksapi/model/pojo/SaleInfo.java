
package com.petprojects.egor.googlebooksapi.model.pojo;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaleInfo implements Serializable {

    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("saleability")
    @Expose
    public String saleability;
    @SerializedName("isEbook")
    @Expose
    public Boolean isEbook;
    @SerializedName("listPrice")
    @Expose
    public ListPrice listPrice;
    @SerializedName("retailPrice")
    @Expose
    public RetailPrice retailPrice;
    @SerializedName("buyLink")
    @Expose
    public String buyLink;
    @SerializedName("offers")
    @Expose
    public List<Offer> offers = null;

}
