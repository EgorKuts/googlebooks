package com.petprojects.egor.googlebooksapi.model.service;

import com.petprojects.egor.googlebooksapi.model.pojo.GoogleBooksApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleBooksApiInterface {
    @GET("books/v1/volumes")
    Call<GoogleBooksApiResponse> getBooks(@Query("q") String query);
}
