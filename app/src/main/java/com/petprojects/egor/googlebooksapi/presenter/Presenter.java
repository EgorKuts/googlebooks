package com.petprojects.egor.googlebooksapi.presenter;

import android.widget.EditText;

import com.petprojects.egor.googlebooksapi.model.pojo.Book;

import java.util.List;

public interface Presenter {
    void searchBook(String string);
    void itemChosen(Book book);
    void setResult(List<Book> arr);
    void setEditTextListener(EditText editText);
    void orientationChanged();
}
