package com.petprojects.egor.googlebooksapi.presenter;

import android.widget.EditText;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;
import com.petprojects.egor.googlebooksapi.exceptions.EmptyQueryResult;
import com.petprojects.egor.googlebooksapi.model.BooksModel;
import com.petprojects.egor.googlebooksapi.model.BooksModelImp;
import com.petprojects.egor.googlebooksapi.model.pojo.Book;
import com.petprojects.egor.googlebooksapi.model.pojo.GoogleBooksApiResponse;
import com.petprojects.egor.googlebooksapi.view.MainView;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class PresenterImp implements Presenter{
    MainView mainView;
    BooksModel booksModel;
    CompositeDisposable disposable = new CompositeDisposable();
    PublishSubject<String> publishSubject = PublishSubject.create();

    public PresenterImp(MainView mainView){
        this.mainView=mainView;
        booksModel=new BooksModelImp(this);
    }

    @Override
    public void searchBook(String string) {
        mainView.startProgressBar();
        try {
            List<Book> result=booksModel.getBooks(string).getItems();
            if(result!=null&&result.size()>0){
                mainView.showSearchResult(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
            mainView.showTost("An network error has occurred");
        }catch (Exception ex){ ex.printStackTrace(); }

        mainView.stopProgressBar();
    }

    @Override
    public void itemChosen(Book book) {
        mainView.openItemActivity(book);
    }

    @Override
    public void setEditTextListener(EditText editText){
        DisposableObserver<GoogleBooksApiResponse> observer = getSearchObserver();

        //getting&handling result
        disposable.add( publishSubject
                        .debounce(250, TimeUnit.MILLISECONDS)
                        .distinctUntilChanged()
                        .switchMapSingle((Function<String, Single<GoogleBooksApiResponse>>)
                                s -> Single.create((SingleOnSubscribe<GoogleBooksApiResponse>)
                                        emitter -> {
                                            try {
                                                GoogleBooksApiResponse response = booksModel.getBooks(s);
                                                if (response != null)
                                                    emitter.onSuccess(response);
                                                else
                                                    emitter.onError(new EmptyQueryResult());
                                            }catch (Exception ex){
                                                    emitter.onError(ex);
                                            }
                                        })
                                        .subscribeOn(Schedulers.computation())
                                        .observeOn(AndroidSchedulers.mainThread()))
                                        .doOnError(throwable -> {
                                            errorHandler(throwable);
                                            disposable.dispose();
                                            disposable.clear();
                                            setEditTextListener(editText);
                                        })
                                        .subscribeWith(observer));

        //getting query
        disposable.add(
                RxTextView.textChangeEvents(editText)
                        .skipInitialValue()
                        .debounce(250, TimeUnit.MILLISECONDS)
                        .filter(s->s.text()!=null&&s.text().length()>0)
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnNext(s->mainView.startProgressBar())
                        .observeOn(Schedulers.io())
                        .subscribeWith(searchBookTextWatcher()));
    }

    @Override
    public void orientationChanged() {
        disposable.dispose();
    }

    private void errorHandler(Throwable throwable){
        System.out.println("Exception handling");
        if (throwable instanceof EmptyQueryResult){
            mainView.showTost("Nothing found =(. Try another query");
        }else {
            mainView.showTost("An network Error has occurred. Try again later");
        }
    }

    private DisposableObserver<GoogleBooksApiResponse> getSearchObserver() {
        return new DisposableObserver<GoogleBooksApiResponse>() {
            @Override
            public void onNext(GoogleBooksApiResponse response) {
                if(response.getItems()!=null&&response.getItems().size()>0) {
                    mainView.stopProgressBar();
                    mainView.showSearchResult(response.getItems());
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
            }
        };
    }

    private DisposableObserver<TextViewTextChangeEvent> searchBookTextWatcher() {
        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                publishSubject.onNext(textViewTextChangeEvent.text().toString());
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
    }


    //for model layer
    @Override
    public void setResult(List<Book> arr) {
    }
}


//        this.disposable.add(RxTextView.textChangeEvents(editText)
//                .skipInitialValue()
//                .debounce(500, TimeUnit.MILLISECONDS)
//                .distinctUntilChanged()
//                .switchMapSingle((Function<TextViewTextChangeEvent, SingleSource<GoogleBooksApiResponse>>) textViewTextChangeEvent ->
//                        Single.just(booksModel.getBooks(textViewTextChangeEvent.text().toString()))
//                                .subscribeOn(Schedulers.io())
//                                .observeOn(AndroidSchedulers.mainThread()))
//                .observeOn(AndroidSchedulers.mainThread())
//                .doOnError(e->{
//                    e.printStackTrace();
//                    mainView.showTost("An network error has occurred");
//                }
//                ).filter(l->l!=null)
//                .subscribe(l->{
//                    mainView.stopProgressBar();
//                    mainView.showSearchResult(l.getItems());
//                }));