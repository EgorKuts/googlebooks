package com.petprojects.egor.googlebooksapi.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.petprojects.egor.googlebooksapi.R;
import com.petprojects.egor.googlebooksapi.model.pojo.Book;

public class ItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        ImageView bookPhotos=findViewById(R.id.fullBookPhotos);
        TextView bookDescription=findViewById(R.id.bookDescription);
        Toolbar toolbar=findViewById(R.id.toolbarBookItem);
        CollapsingToolbarLayout collapsingToolbarLayout=findViewById(R.id.collapsingToolBar);
        Button moreInfoAboutBook=findViewById(R.id.moreAboutBook);
        Button bookPreview=findViewById(R.id.bookPreview);
        RatingBar ratingBar=findViewById(R.id.bookRating);
        AppBarLayout appBarLayout=findViewById(R.id.appbar);

        Book book=(Book) getIntent().getSerializableExtra("BOOK");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(book.getVolumeInfo().getTitle());
        Log.d("title:",book.getVolumeInfo().getTitle());

        toolbar.setNavigationIcon(R.drawable.ic_stat_name);
        toolbar.setNavigationOnClickListener(view -> finish());

        String description=book.getVolumeInfo().getDescription();

        if(description!=null&&description.length()>0)
        bookDescription.setText(book.getVolumeInfo().getDescription());
        else bookDescription.setText("Description isn't provided");

        if(book.getVolumeInfo().getImageLinks()!=null)
        Glide.with(this).load(book.getVolumeInfo().getImageLinks().thumbnail).into(bookPhotos);

        moreInfoAboutBook.setOnClickListener(v->{
            //to open web browser
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(book.getVolumeInfo().getInfoLink()));
            startActivity(i);
        });

        bookPreview.setOnClickListener(v->{
            Intent i=new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(book.getVolumeInfo().getPreviewLink()));
            startActivity(i);
        });

        ratingBar.setRating(book.getVolumeInfo().getAverageRating()==null?0:book.getVolumeInfo().getAverageRating());

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener(){
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(book.getVolumeInfo().getTitle());
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }

}
