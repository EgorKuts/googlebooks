package com.petprojects.egor.googlebooksapi.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.petprojects.egor.googlebooksapi.R;
import com.petprojects.egor.googlebooksapi.adapters.BooksAdapter;
import com.petprojects.egor.googlebooksapi.adapters.SearchBooksAdapter;
import com.petprojects.egor.googlebooksapi.model.pojo.Book;

import com.petprojects.egor.googlebooksapi.view.decorators.BooksRecyclerViewDecorator;
import com.petprojects.egor.googlebooksapi.presenter.Presenter;
import com.petprojects.egor.googlebooksapi.presenter.PresenterImp;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainView, SearchBooksAdapter.ItemClickCallback {
    RecyclerView recyclerView;
    BooksAdapter booksAdapter;
    ProgressBar progressBar;
    EditText searchQuery;

    @Inject Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.getBackground().setAlpha(0);

        setSupportActionBar(toolbar);

        progressBar=findViewById(R.id.progressbar);

        searchQuery=findViewById(R.id.input_search);

        booksAdapter=new SearchBooksAdapter(this, book -> presenter.itemChosen(book));

        recyclerView=findViewById(R.id.recycler_view);
        recyclerView.setAdapter((RecyclerView.Adapter) booksAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.addItemDecoration(new BooksRecyclerViewDecorator(5));

        presenter=new PresenterImp(this);
        presenter.setEditTextListener(searchQuery);
        Log.d("CurrentThreadActivity",Thread.currentThread().getName());
    }

    @Override
    public void startProgressBar(){
        Log.d("DebugPoint",Thread.currentThread().getName());
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void stopProgressBar() {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
    }

    @Override
    public void showTost(String text){
        Log.d("CurrentThread",Thread.currentThread().getName());
        Toast.makeText(this,text,Toast.LENGTH_LONG).show();
    }

    @Override
    public void click(Book book) {
        presenter.itemChosen(book);
    }

    public void openItemActivity(Book book){
        Intent intent = new Intent(this, ItemActivity.class);
        intent.putExtra("BOOK",book);
        intent.putExtra("CURRENT_SEARCH_QUERY",searchQuery.getText().toString());
        startActivity(intent);
    }

    @Override
    public void showSearchResult(List<Book> books){
        Log.d("bookCount"," "+books.size());
        booksAdapter.addBooks(books);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        presenter.orientationChanged();
    }
}
