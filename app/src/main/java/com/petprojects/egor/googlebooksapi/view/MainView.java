package com.petprojects.egor.googlebooksapi.view;

import com.petprojects.egor.googlebooksapi.model.pojo.Book;

import java.util.List;

public interface MainView {
    void startProgressBar();
    void stopProgressBar();
    void showTost(String text);//if an network exception occur, we should throw alert
    void showSearchResult(List<Book> books);
    void openItemActivity(Book book);
}
