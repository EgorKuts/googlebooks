package com.petprojects.egor.googlebooksapi;

import com.petprojects.egor.googlebooksapi.model.BooksModel;
import com.petprojects.egor.googlebooksapi.model.BooksModelImp;
import com.petprojects.egor.googlebooksapi.model.pojo.Book;
import com.petprojects.egor.googlebooksapi.model.pojo.GoogleBooksApiResponse;
import com.petprojects.egor.googlebooksapi.presenter.Presenter;
import com.petprojects.egor.googlebooksapi.presenter.PresenterImp;
import com.petprojects.egor.googlebooksapi.view.MainView;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GoogleApiServiceTest {
    BooksModel booksModel;

    @Before
    public void initiateBooksModel(){

        booksModel=new BooksModelImp(new PresenterImp(new MainView() {
            @Override
            public void startProgressBar() {
                System.out.println(Thread.currentThread().getName());
            }

            @Override
            public void stopProgressBar() {
                System.out.println(Thread.currentThread().getName());
            }

            @Override
            public void showTost(String text) {

            }

            @Override
            public void showSearchResult(List<Book> books) {

            }

            @Override
            public void openItemActivity(Book book) {

            }
        }));
    }

    @Test
    public void takingBooksByQuery() throws IOException {
        GoogleBooksApiResponse response= booksModel.getBooks("Java");
        System.out.println(response.totalItems);
    }

    @Test
    public void requestBookByInvalidQuery() throws IOException {
        GoogleBooksApiResponse response= booksModel.getBooks("bjklbsniuerowoebi");
    }


    @Test
    public void convertLanguageNameByLanguageCode(){
        Locale deLocale = new Locale("ru", "RU");
        System.out.println(deLocale.getDisplayLanguage());
    }
}
